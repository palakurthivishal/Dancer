'use strict';

/**
 * @ngdoc overview
 * @name dancerApp
 * @description
 * # dancerApp
 *
 * Main module of the application.
 */
angular
    .module('dancerApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'ui.router',
        'anim-in-out',
        'ui.bootstrap',
        'youtube-embed'
    ])
    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        $urlRouterProvider.otherwise('/home'); // URL to be routed when the URL is irrelevant to the application

        $stateProvider.state('home', {
            url: '/home',
            templateUrl: 'views/homeView.html',
            controller: 'HomeController'
        }).state('performances', {
            url: '/performances',
            templateUrl: 'views/performancesView.html',
            controller: 'PerformancesController'
        }).state('awards', {
            url: '/awards',
            templateUrl: 'views/awardsView.html',
            controller : 'AwardsController'
        }).state('photos', {
            url: '/photos',
            templateUrl: 'views/photosView.html',
            controller: 'PhotosController'
        }).state('videos', {
            url: '/videos',
            templateUrl: 'views/videosView.html',
            controller: 'VideosController'
        }).state('reviews', {
            url: '/reviews',
            templateUrl: 'views/reviewsView.html',
            controller : 'ReviewsController'
        }).state('contact', {
            url: '/contact',
            templateUrl: 'views/contactView.html',
            controller: 'ContactController'
        });
    }).run(function($rootScope) {
        $rootScope.$on('$stateChangeStart', function() {
            window.scrollTo(0,0);
        });
    });
