'use strict';
angular.module('dancerApp').directive('spinnerOnLoad', [function() {
    return {
        restrict: 'A',
        link: function(scope, element,attrs) {
            element.css('opacity',0);
            element.on('load', function() {
                element.removeClass('spinner-hide');
                element.addClass('spinner-show');
                element.parent().find('span').remove();
                element.css('opacity',1);
            });
        }
    };
}])
