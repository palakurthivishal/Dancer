'use strict';
angular.module('dancerApp')
    .controller('HomeController', function($scope,AppContext) {
    	$scope.slides = angular.copy(AppContext.HOME_VIEW.CAROUSEL_IMAGES);
    });
