angular.module('dancerApp').controller('ContactController', function($scope, $http, AppContext) {
    $scope.contactObj = {};
    $scope.submitForm = function() {
        var formData = $scope.contactObj;
        if (!formData.name) {
            alert('Please enter your name');
            return;
        }
        if (!formData.mobile) {
            alert('Please enter your mobile number');
            return;
        }
        if (!formData.email) {
            alert('Please enter your email address');
            return;
        }
        if (!formData.message) {
            alert('Please enter message');
            return;
        }
             var sendEmail = {
            url: 'https://mandrillapp.com/api/1.0/messages/send.json',
            method: 'POST',
            data: {
                "key": "BVtAo9UBZH7bqSWMYe3eAQ",
                "message": {
                    "html": formData.message,
                    "text": 'Name : ' + formData.name + ', Mobile : ' + formData.mobile + ', Email : ' + formData.email + ', Message : ' + formData.message,
                    "subject": "Contact Request",
                    "from_email": "palakurthivishal@gmail.com",
                    "from_name": "Vishal Palakurthi",
                    "to": [{
                        "email": 'contact@lalithasindhuri.com',
                        "name": formData.name,
                        "type": "to"
                    }]
                }
            }
        };

        $http(sendEmail).success(function() {
            alert('Your request has been submitted to Ms.Lalitha Sindhuri');
            $scope.contactObj= {};
        }).error(function() {
            alert('Failed to send your request');
            //TODO : proper message to the user about the reason of failure.
        });
    };
});
