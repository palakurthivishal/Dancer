angular.module('dancerApp').controller('PhotosController', function($scope, AppContext) {
	$scope.images = angular.copy(AppContext.PHOTOS_VIEW.IMAGES);
	$scope.expandImage = function(index){
		var temp = angular.copy($scope.images);
		var subArr = temp.slice(index,temp.length);
		temp = temp.slice(0,index);
		temp = subArr.concat(temp);
		$scope.carousel_images = temp;
		$('#photo-expand-modal').modal('show');
	};
});
