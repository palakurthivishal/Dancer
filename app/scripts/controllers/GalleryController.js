'use strict';
angular.module('dancerApp').controller('GalleryController', function($scope, $sce, $timeout, $interval,AppContext) {
    var videoUrl = 'http://www.thebigdot.com/1234.mp4';
    $scope.videoUrl = $sce.trustAsResourceUrl(videoUrl);
    $scope.slides = angular.copy(AppContext.GALLERY_VIDEO_THUMBS);
    $scope.selectedTab = 'images';

    $scope.setVideo = function(videoUrl){
    	$scope.videoUrl = $sce.trustAsResourceUrl(videoUrl);
    	$timeout(function() {
    		$('#video-player')[0].play();
    	}, 100);
    };
    $scope.setSelectedTab = function(tab){
    	$scope.selectedTab = tab;
    };

    $scope.images = angular.copy(AppContext.GALLERY_IMAGES);
    
});
