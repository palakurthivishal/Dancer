'use strict';
angular.module('dancerApp').controller('PerformancesController', function($scope,AppContext){
	$scope.upcomingEvents = angular.copy(AppContext.PERFORMANCES_VIEW.UPCOMING_EVENTS);
	$scope.achievements = angular.copy(AppContext.PERFORMANCES_VIEW.ACHIEVEMENTS);
	$scope.majorEvents = angular.copy(AppContext.PERFORMANCES_VIEW.MAJOR_EVENTS);
});