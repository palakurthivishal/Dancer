'use strict';
angular.module('dancerApp').controller('RootController',function($scope,$rootScope,$state,AppContext){
	$scope.left_menus = angular.copy(AppContext.LEFT_MENUS);
	$scope.activateMenu = function(stateName){
		var state = $state.current.name;
		return stateName === state;
	};
	$rootScope.isAudioPaused = false;
	$scope.isAudioMuted = false;
	$scope.playAudio = function(){
		var elt = angular.element('#audioEl')[0];
		$rootScope.isAudioPaused = false;
		elt.play();
	};
	$scope.pauseAudio = function(){
		var elt = angular.element('#audioEl')[0];
		$rootScope.isAudioPaused = true;
		elt.pause();
	};
	$scope.muteAudio = function(){
		var elt = angular.element('#audioEl')[0];
		$scope.isAudioMuted = true;
		elt.muted = true;
	};
	$scope.unmuteAudio = function(){
		var elt = angular.element('#audioEl')[0];
		$scope.isAudioMuted = false;
		elt.muted = false;
	};
});