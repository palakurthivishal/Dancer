angular.module('dancerApp').controller('VideosController', function($scope, $sce,$rootScope, AppContext) {
    $scope.video_thumbs = angular.copy(AppContext.VIDEOS_VIEW.VIDEO_THUMBS);
    $scope.videoId = $scope.video_thumbs[0].thumbs[0].videoId;
    $scope.playerVariables = {
        autoplay: 1
    };
    var audioEl = $('#audioEl')[0];
    if (audioEl){
    	$rootScope.isAudioPaused = true;
        audioEl.pause();
    }
    $scope.playVideo = function(videoId) {
        $scope.videoId = videoId;
    };
});
