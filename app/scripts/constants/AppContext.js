'use strict';
angular.module('dancerApp').constant('AppContext', {
    'DEFAULT_ROUTE': 'dancer',
    'LEFT_MENUS': [{
        'name': 'Profile',
        'hash': 'home',
        'icon': 'user'
    }, {
        'name': 'Performances',
        'hash': 'performances',
        'icon': 'gittip'
    }, {
        'name': 'Awards',
        'hash': 'awards',
        'icon': 'star'
    }, {
        'name': 'Photos',
        'hash': 'photos',
        'icon': 'camera'
    }, {
        'name': 'Videos',
        'hash': 'videos',
        'icon': 'play'
    }, {
        'name': 'Reviews',
        'hash': 'reviews',
        'icon': 'users'
    }, {
        'name': 'Contact',
        'hash': 'contact',
        'icon': 'envelope'
    }],
    'HOME_VIEW': {
        'CAROUSEL_IMAGES': [{
            'src': 'images/dancer1.jpg',
            'desc': 'Performance at so and so'
        }, {
            'src': 'images/dancer2.jpg',
            'desc': 'Performance at so and so'
        }, {
            'src': 'images/dancer3.jpg',
            'desc': 'Performance at so and so'
        }, {
            'src': 'images/dancer7.jpg',
            'desc': 'Performance at so and so'
        }]
    },
    'PERFORMANCES_VIEW': {
        'UPCOMING_EVENTS': [{
            'description': 'Performing for Sri Natyanjali Kala Academy, Manglore on September 13th, 2015.'
        }],
        'ACHIEVEMENTS': [{
            'description': 'First Prize in the Dance competitions ‘SiriSiriMuvva’ conducted by Samskriti Channel and Dr. Sobha Naidu.'
        }, {
            'description': '(YMCA Hyderabad) Secured first prize in the National Level Dance Competitions for four consecutive years 2007,2008,2009 and 2010 and received silver rolling trophy by Navya Nataka Samithi'
        }, {
            'description': 'First prize in the national level competitions in Nellore by Adalanritya Sankalp'
        }, {
            'description': 'Secured first in the competition conducted by Doordarshan “Muvvala Savvadi”'
        }, {
            'description': 'Given many performances for the prestigious organization ‘Rasamayi’'
        }],
        'MAJOR_EVENTS': [{
            'description': 'National Dance Festival by Regatta Cutural Association, Trivandrum , Kerala'
        },{
            'description': 'Kalabharati National Dance Festival, Trissur, Kerala'
        },{
            'description': 'National Dance Festival by Nrityathi Kalakshetra, Bhilai'
        },{
            'description': 'Andhra Pradesh Paper Mills, International Conference, Hyderabad'
        },{
            'description': 'Nadaneerajanam - Performance given in the prestigious Nadaneerajanam Programme at Tirumala SrivariSannidhi on 27 - 03 - 2011.'
        }, {
            'description': 'Sangitha Vidvath Sabha - Performances given in the above prestigious 64th and Kakinada 65 th Anniversary Functions in 2010 & 2011 '
        }, {
            'description': 'Srivari Brahmotsavam - Performance given in Srivari Brahmotsavam Festivals Tirupathi on 09 - 10 - 2010 '
        }, {
            'description': 'Tyagaraja Narayanadasa - Performance given in the prestigious 60th Anniversary Sevasamithi, Rajahmundry Functions on 05 - 04 - 2011 '
        }, {
            'description': 'Gajapathi Vutsav - Performance given in the Odisha State Festival on Odisha State 09 - 01 - 2011 '
        }, {
            'description': 'Tanisha Yuva Vutsav, 2011:     Performance given in the above Festival on 18-03-2011'
        }, {
            'description': 'Mangoliya Utsav - Invited for Mangoliya Utsav - 2006 from National Balabhavan, New Delhi'
        }, {
            'description': 'Sirisirimuvva 2007 - Received First Prize from Kalatapasvi K. Viswanadh in the Competition conducted by Padmasri Dr.Sobhanaidu in Samskruthi T.V. and Tv9'
        }, {
            'description': 'Navya Nataka Samithi - Received First Prizes in the National Level Dance Hyderabad Competitions for 2004, 2005, 2006, 2007 consecutively from Dr. Mangalampalli Balamurali Krishna and K. Viswanadh'
        }, {
            'description': 'Andhra Natakasamithi - Received First Prize and Rs.10,000/- Cash Award in Vijayawada State Level Kuchipudi Dance Competitions on 25-11-2005'
        }]
    },
    'PHOTOS_VIEW': {
        'IMAGES': [{
            'src': 'images/dancer1.jpg',
            'thumb': 'images/thumbs/dancer1.jpg'
        }, {
            'src': 'images/dancer2.jpg',
            'thumb': 'images/thumbs/dancer2.jpg'
        }, {
            'src': 'images/dancer3.jpg',
            'thumb': 'images/thumbs/dancer3.jpg'
        }, {
            'src': 'images/dancer4.jpg',
            'thumb': 'images/thumbs/dancer4.jpg'
        }, {
            'src': 'images/dancer5.jpg',
            'thumb': 'images/thumbs/dancer5.jpg'
        }, {
            'src': 'images/dancer6.jpg',
            'thumb': 'images/thumbs/dancer6.jpg'
        }, {
            'src': 'images/dancer7.jpg',
            'thumb': 'images/thumbs/dancer7.jpg'
        }, {
            'src': 'images/dancer8.jpg',
            'thumb': 'images/thumbs/dancer8.jpg'
        }, {
            'src': 'images/dancer9.jpg',
            'thumb': 'images/thumbs/dancer9.jpg'
        }, {
            'src': 'images/dancer10.jpg',
            'thumb': 'images/thumbs/dancer10.jpg'
        }, {
            'src': 'images/dancer11.jpg',
            'thumb': 'images/thumbs/dancer11.jpg'
        }, {
            'src': 'images/dancer12.jpg',
            'thumb': 'images/thumbs/dancer12.jpg'
        }, {
            'src': 'images/dancer13.jpg',
            'thumb': 'images/thumbs/dancer13.jpg'
        }, {
            'src': 'images/dancer14.jpg',
            'thumb': 'images/thumbs/dancer14.jpg'
        }, {
            'src': 'images/dancer15.jpg',
            'thumb': 'images/thumbs/dancer15.jpg'
        }, {
            'src': 'images/dancer16.jpg',
            'thumb': 'images/thumbs/dancer16.jpg'
        }, {
            'src': 'images/dancer17.jpg',
            'thumb': 'images/thumbs/dancer17.jpg'
        }, {
            'src': 'images/dancer18.jpg',
            'thumb': 'images/thumbs/dancer18.jpg'
        }, {
            'src': 'images/dancer19.jpg',
            'thumb': 'images/thumbs/dancer19.jpg'
        }, {
            'src': 'images/dancer20.jpg',
            'thumb': 'images/thumbs/dancer20.jpg'
        }, {
            'src': 'images/dancer21.jpg',
            'thumb': 'images/thumbs/dancer21.jpg'
        }, {
            'src': 'images/dancer22.jpg',
            'thumb': 'images/thumbs/dancer22.jpg'
        }, {
            'src': 'images/dancer23.jpg',
            'thumb': 'images/thumbs/dancer23.jpg'
        }, {
            'src': 'images/dancer24.jpg',
            'thumb': 'images/thumbs/dancer24.jpg'
        }, {
            'src': 'images/dancer25.jpg',
            'thumb': 'images/thumbs/dancer25.jpg'
        }, {
            'src': 'images/dancer26.jpg',
            'thumb': 'images/thumbs/dancer26.jpg'
        }, {
            'src': 'images/dancer27.jpg',
            'thumb': 'images/thumbs/dancer27.jpg'
        }, {
            'src': 'images/dancer28.jpg',
            'thumb': 'images/thumbs/dancer28.jpg'
        }, {
            'src': 'images/dancer29.jpg',
            'thumb': 'images/thumbs/dancer29.jpg'
        }, {
            'src': 'images/dancer30.jpg',
            'thumb': 'images/thumbs/dancer30.jpg'
        }, {
            'src': 'images/dancer31.jpg',
            'thumb': 'images/thumbs/dancer31.jpg'
        }, {
            'src': 'images/dancer32.jpg',
            'thumb': 'images/thumbs/dancer32.jpg'
        }, {
            'src': 'images/dancer33.jpg',
            'thumb': 'images/thumbs/dancer33.jpg'
        }, {
            'src': 'images/dancer34.jpg',
            'thumb': 'images/thumbs/dancer34.jpg'
        }, {
            'src': 'images/dancer35.jpg',
            'thumb': 'images/thumbs/dancer35.jpg'
        }, {
            'src': 'images/dancer36.jpg',
            'thumb': 'images/thumbs/dancer36.jpg'
        }, {
            'src': 'images/dancer37.jpg',
            'thumb': 'images/thumbs/dancer37.jpg'
        }]
    },
    'AWARDS_VIEW': {
        'IMAGES': [{
            'src': 'images/award1.jpg',
            'thumb': 'images/thumbs/award1.jpg',
            'description': 'Received from the Chief Minister Chandrababu Naidu'
        }, {
            'src': 'images/award2.jpg',
            'thumb': 'images/thumbs/award2.jpg',
            'description': 'Received from the Late Chief Minister Dr. Y.S.Rajasekhara Reddy.'
        }, {
            'src': 'images/award3.jpg',
            'thumb': 'images/thumbs/award3.jpg',
            'description': 'Received from the Late President Dr. A.P.J Abdul Kalam.'
        }]
    },
    'VIDEOS_VIEW': {
        'VIDEO_THUMBS': [{
            'thumbs': [{
                'src': '//i.ytimg.com/vi_webp/Z6fATms_dUs/default.webp',
                'videoId': 'Z6fATms_dUs'
            }, {
                'src': '//i.ytimg.com/vi/jkHd7HlYAxY/mqdefault.jpg',
                'videoId': 'jkHd7HlYAxY'
            }, {
                'src': '//i.ytimg.com/vi_webp/yJi_MYcXX4Y/default.webp',
                'videoId': 'yJi_MYcXX4Y'
            }, {
                'src': '//i.ytimg.com/vi/0wWQLjwNJAw/mqdefault.jpg',
                'videoId': '0wWQLjwNJAw'
            }, {
                'src': '//i.ytimg.com/vi_webp/VaXc02Munu8/default.webp',
                'videoId': 'VaXc02Munu8'
            }, {
                'src': '//i.ytimg.com/vi_webp/S_9iOYaO9ww/default.webp',
                'videoId': 'S_9iOYaO9ww'
            }]
        }, {
            'thumbs': [{
                'src': '//i.ytimg.com/vi/yd-39OtoH1g/mqdefault.jpg',
                'videoId': 'yd-39OtoH1g'
            }, {
                'src': '//i.ytimg.com/vi/3Oid_GuMFhw/mqdefault.jpg',
                'videoId': '3Oid_GuMFhw'
            }, {
                'src': '//i.ytimg.com/vi/b6qdQB3vtY4/mqdefault.jpg',
                'videoId': 'b6qdQB3vtY4'
            }, {
                'src': '//i.ytimg.com/vi/FmAkyJhwEBg/mqdefault.jpg',
                'videoId': 'FmAkyJhwEBg'
            }, {
                'src': '//i.ytimg.com/vi/J-HGvUTC2nA/mqdefault.jpg',
                'videoId': 'J-HGvUTC2nA'
            }, {
                'src': '//i.ytimg.com/vi/YrrAGp9gigI/mqdefault.jpg',
                'videoId': 'YrrAGp9gigI'
            }]
        }, {
            'thumbs': [{
                'src': '//i.ytimg.com/vi/LX60OGXhYrs/mqdefault.jpg',
                'videoId': 'LX60OGXhYrs'
            }]
        }]
    },
    'REVIEWS_VIEW': {
        'IMAGES': [{
            'src': 'images/review1.jpg',
            'thumb': 'images/thumbs/review1.jpg'
        }, {
            'src': 'images/review2.jpg',
            'thumb': 'images/thumbs/review2.jpg'
        }, {
            'src': 'images/review3.jpg',
            'thumb': 'images/thumbs/review3.jpg'
        }, {
            'src': 'images/review4.jpg',
            'thumb': 'images/thumbs/review4.jpg'
        }, {
            'src': 'images/review5.jpg',
            'thumb': 'images/thumbs/review5.jpg'
        }, {
            'src': 'images/review6.jpg',
            'thumb': 'images/thumbs/review6.jpg'
        }]
    }

});
